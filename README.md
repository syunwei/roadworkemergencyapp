# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Roadwork Emergency App is a front-end application for Roadwork Emergency System.
* Version 1.0

### How do I get set up? ###

* Setting up server's URI: go to “REHTTPRequest.h” file, change the following code to your own server URI.

```
#!objective-c

#define SERVER_URI @"http://58.173.165.193:8080/emergencynotificationserver"
```
  


* Dependencies: AFNetworking is used in http request for connecting to server which has already included in the project source code. Alternatively, you can use CocoaPods to manage your dependencies.


### Additional information ###
Here are some knowledge you may need for maintaining this App:
* Objective-C: yes, it is an native iOS application.

* MapKit: Applel maps is used in this App which is provided in iOS SDK. If you want to change maps to Google Map, please refer to [Google Maps SDK for iOS](https://developers.google.com/maps/documentation/ios-sdk/).
* Push Notification: here is a document from Apple [Apple Push Notification Service](https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/ApplePushService.html) 

* Enable to push notification from your server to devices; you need to set up certifications both on developer’s member centre and your server. 
Here is an good [tutorial](http://www.raywenderlich.com/32960/apple-push-notification-services-in-ios-6-tutorial-part-1).
* AFNetworking: for http request mainly used in connecting to your server.