//
//  GEOFencinCalculator.m
//  Patient Care
//
//  Created by Syunwei on 9/16/14.
//  Copyright (c) 2014 Syunwei. All rights reserved.
//

#import "GEOFencinCalculator.h"
#import <CoreLocation/CLCircularRegion.h>
#include <math.h>

const double PIx = 3.141592653589793;
const double RADIO = 6371; //radius of Earth in Km


@implementation GEOFencinCalculator

+(BOOL) isLocation:(CLLocation*)location outOfFence:(CLRegion*)region
{
    
    
    if ([region isKindOfClass:[CLCircularRegion class]])
    {
        CLCircularRegion *circularRegion = (CLCircularRegion*)region;
        CLLocation *fecneCenter = [[CLLocation alloc] initWithLatitude:circularRegion.center.latitude longitude:circularRegion.center.longitude];
        double radious = circularRegion.radius;
        CLLocationDistance distance = [GEOFencinCalculator calculateDistanceBetween:location and:fecneCenter];
        if (distance - radious > 0)
        {
            //out of fence
            return YES;
        }
    }
    
    return NO;
}

double convertToRadians(double val) {
    
    return val * PIx / 180.f;
}

//+(CLLocationDistance) calculateDistanceBetween:(CLLocationCoordinate2D)coordinate1 and:(CLLocationCoordinate2D)coordinate2
//{
////    double lat1 = coordinate1.latitude;
////    double lon1 = coordinate1.longitude;
////    double lat2 = coordinate2.latitude;
////    double lon2 = coordinate2.longitude;
//    
////    double dlon = lon2 - lon1;
////    double dlat = lat2 - lat1;
////    double a = pow((sin(dlat/2.f)), 2.f) + cos(lat1) * cos(lat2) * pow(sin(dlon/2.f),2.f);
////    double c = 2.f * atan2( sqrt(a), sqrt(1.f-a) );
////    double d = RADIOUS_OF_THE_EARTH * c;
////    return d;
//    double dlon = convertToRadians(coordinate2.longitude - coordinate1.longitude);
//    double dlat = convertToRadians(coordinate2.latitude - coordinate1.latitude);
//    
//    double a = ( pow(sin(dlat / 2.f), 2) + cos(convertToRadians(coordinate1.latitude))) * cos(convertToRadians(coordinate2.latitude)) * pow(sin(dlon / 2.f), 2);
//    double angle = 2.f * asin(sqrt(a));
//    
//    return angle * RADIO * 1000.f;
//    
//
//}

+(CLLocationDistance) calculateDistanceBetween:(CLLocation*)location1 and:(CLLocation*)location2
{
    return [location1 distanceFromLocation:location2];
}

@end
