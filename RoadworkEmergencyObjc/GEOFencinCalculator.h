//
//  GEOFencinCalculator.h
//  Patient Care
//
//  Created by Syunwei on 9/16/14.
//  Copyright (c) 2014 Syunwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLRegion.h>
#import <CoreLocation/CLLocation.h>

@interface GEOFencinCalculator : NSObject

+(BOOL) isLocation:(CLLocation*)location outOfFence:(CLRegion*)region;
+(CLLocationDistance) calculateDistanceBetween:(CLLocation*)location1 and:(CLLocation*)location2;

@end
