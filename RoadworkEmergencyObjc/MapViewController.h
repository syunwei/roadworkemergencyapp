//
//  FirstViewController.h
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 4/18/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "REHTTPRequest.h"

@class User;

@interface MapViewController : UIViewController <CLLocationManagerDelegate,REHTTPRequestDelegate,MKMapViewDelegate>
{
    IBOutlet MKMapView *_mapView;
    REHTTPRequest *roadworkRequest;
    REHTTPRequest *userlocationRequest;
    User *user;
    NSArray * roadworks;
    NSMutableArray * roadworkPOIs;
    BOOL firstTimeLoadMap;
    NSMutableArray *fences;
    NSMutableArray * alertedRoadworkIds; // put the roadwork id which has been alerted in the array
}

@property(nonatomic,strong) CLLocationManager *locationManager;

@end

