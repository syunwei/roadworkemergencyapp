//
//  REFectory.m
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 5/3/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import "REFectory.h"
#import "Roadwork.h"

@implementation REFectory

+(Roadwork*) createRoadwork:(NSDictionary*)dictionary
{
    Roadwork *roadwork = [[Roadwork alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d, yyyy"];
    
    long roadworkid = [dictionary[@"roadworkId"] longValue];
    BOOL isHighImpact = [dictionary[@"isHighImpact"] boolValue];
    NSString *name = dictionary[@"name"];
    NSString *delay = dictionary[@"delay"];
    NSString *roadworkDescription = dictionary[@"description"];
    NSString *advice = dictionary[@"advice"];
    NSString *extralDetails = dictionary[@"extralDetails"];
    NSDictionary *geometry = dictionary[@"geometry"];
    
    
    NSDictionary *temporal = dictionary[@"temporal"];
    
    if (temporal)
    {
        NSDate * startDate = [dateFormatter dateFromString:temporal[@"startDate"]];
        NSDate * endDate = [dateFormatter dateFromString:temporal[@"endDate"]];
        
        roadwork.startDate = startDate;
        roadwork.endDate = endDate;
    }
    
    
    roadwork.roadworkId = roadworkid;
    roadwork.isHighImpact = isHighImpact;
    roadwork.name = name;
    roadwork.delay = delay;
    roadwork.roadWorkDescription = roadworkDescription;
    roadwork.advice = advice;
    
    roadwork.extraDetails = extralDetails;
    
    
    if (geometry)
    {
        NSString* geometryType = geometry[@"type"];
        NSString* address = geometry[@"address"];
        if ([geometryType isEqualToString:@"Point"])
        {
            NSDictionary *location = geometry[@"location"];
            roadwork.point = CLLocationCoordinate2DMake([location[@"y"] doubleValue],[location[@"x"] doubleValue]);
        }
        roadwork.address = address;
    }
    
    
    
    
    return roadwork;
}

@end
