//
//  RoadworkPOI.h
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 5/5/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "Roadwork.h"

@interface RoadworkPOI : NSObject<MKAnnotation>

@property (nonatomic,assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *advice;
@property(nonatomic, strong) UIImage* image;
@property (nonatomic, strong) CLCircularRegion* fence;
@property (nonatomic, strong) Roadwork* roadwork;

@end
