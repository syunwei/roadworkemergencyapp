//
//  Roadwork.h
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 5/3/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Roadwork : NSObject

@property (nonatomic) long roadworkId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic) BOOL isHighImpact;
@property (nonatomic, strong) NSString* roadWorkDescription;
@property (nonatomic, strong) NSString* delay;
@property (nonatomic, strong) NSString* advice;
@property (nonatomic, strong) NSString* extraDetails;
@property (nonatomic, assign) CLLocationCoordinate2D point;
@property (nonatomic, strong) NSString* address;
@property (nonatomic, strong) NSDate* startDate;
@property (nonatomic, strong) NSDate* endDate;

@end
