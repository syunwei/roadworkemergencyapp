//
//  EmergencyViewController.m
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 5/26/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import "EmergencyViewController.h"

@implementation EmergencyViewController


-(void) showConfirnmation
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Do you want to make an Emergency call?" message:@"" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alertView show];
}

- (IBAction)emergencyCallButtonPressed:(id)sender
{
    [self showConfirnmation];
}

-(void) makeEmergencyCall
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:000"]];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self makeEmergencyCall];
    }
}



@end
