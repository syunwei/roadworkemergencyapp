//
//  REFectory.h
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 5/3/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Roadwork;

@interface REFectory : NSObject

+(Roadwork*) createRoadwork:(NSDictionary*)dictionary;

@end
