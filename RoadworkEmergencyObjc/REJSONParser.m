//
//  REJSONParser.m
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 5/3/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import "REJSONParser.h"
#import "REFectory.h"
@class Roadwork;

@implementation REJSONParser



+(NSMutableArray*) parseRoadworks:(NSDictionary*)json
{
    NSMutableArray *roadworks = [NSMutableArray array];
    NSArray *results = [json objectForKey:@"roadworks"];
    for (NSDictionary* roadworkDict in results)
    {
        if ([roadworkDict isKindOfClass:[NSDictionary class]])
        {
            Roadwork *roadwork = [REFectory createRoadwork:roadworkDict];
            [roadworks addObject:roadwork];
        }
    }
    
    
    return roadworks;
}

@end
