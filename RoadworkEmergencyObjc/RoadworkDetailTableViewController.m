//
//  RoadworkDetailTableViewController.m
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 5/14/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import "RoadworkDetailTableViewController.h"
#import "Roadwork.h"
#import "REHTTPRequest.h"

@interface RoadworkDetailTableViewController ()

@end

@implementation RoadworkDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    httpRequest = [[REHTTPRequest alloc] init];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self setupBarButtonItems];
    
    [self loadInput:_roadwork];
}

-(void) setupBarButtonItems
{
    if (_editingMode)
    {
        UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
        self.navigationItem.rightBarButtonItem = doneBarButton;
    }
    
    UIBarButtonItem *cancelBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonPressed)];
    self.navigationItem.leftBarButtonItem = cancelBarButton;
}

-(void) doneButtonPressed
{
    if (_editingMode)
    {
        // put roadwork
        _roadwork.name = nameTextField.text;
        _roadwork.delay = delayTextField.text;
        _roadwork.address = addressTextField.text;
        _roadwork.roadWorkDescription = descriptionTextView.text;
        [httpRequest putRoadwork:_roadwork];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) cancelButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) setEditingMode:(BOOL)editable
{
    _editingMode = editable;
    [descriptionTextView setEditable:editable];
}

-(void) loadInput:(Roadwork*)roadwork
{
    if (roadwork.name)
    {
        nameTextField.text = roadwork.name;
    }
    
    if (roadwork.delay)
    {
        delayTextField.text = roadwork.delay;
    }
    
    if (roadwork.address)
    {
        addressTextField.text = roadwork.address;
    }
    
    locationTextField.text = [NSString stringWithFormat:@"%f,%f",roadwork.point.longitude, roadwork.point.latitude];
    
    if (roadwork.roadWorkDescription)
    {
        descriptionTextView.text = roadwork.roadWorkDescription;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 5;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return _editingMode;
}

#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return _editingMode;
}

@end
