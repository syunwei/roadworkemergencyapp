//
//  REGTTPRequest.h
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 4/18/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AFHTTPRequestOperationManager, User, Roadwork;
@protocol REHTTPRequestDelegate;


@interface REHTTPRequest : NSObject
{
    AFHTTPRequestOperationManager *_httpManager;
    User *user;
}

@property(nonatomic,weak)id <REHTTPRequestDelegate> delegate;

+(instancetype) request;
-(void) postToken:(NSString*) token;
-(void) fetchRoadworksFromLon:(double)longitude Lat:(double)latitude radius:(float)radius;
-(void) postUserLocation;
-(void)putRoadwork:(Roadwork*)roadwork;

@end

@protocol REHTTPRequestDelegate <NSObject>

-(void)request:(REHTTPRequest*)httpRequest receiveJSON:(NSDictionary*)json;

@end