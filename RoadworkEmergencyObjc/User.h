//
//  User.h
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 4/18/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject


@property(nonatomic) long userid;
@property(nonatomic) NSString *username;
@property(nonatomic) NSString *deviceid;
@property(nonatomic,readonly) int platformid;
@property(nonatomic) float radius;
@property(nonatomic) double longitude;
@property(nonatomic) double latitude;

+ (id)sharedUser;

@end
