//
//  NSString+Base64.h
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 4/18/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSStringAdditions)

+ (NSString *) base64StringFromData:(NSData *)data length:(int)length;


@end

@interface NSData (NSDataAdditions)

+ (NSData *) base64DataFromString:(NSString *)string;

@end
