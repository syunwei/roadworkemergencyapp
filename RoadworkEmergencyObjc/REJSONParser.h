//
//  REJSONParser.h
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 5/3/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Roadwork.h"

@interface REJSONParser : NSObject


+(NSMutableArray*) parseRoadworks:(NSDictionary*)JSONString;

@end
