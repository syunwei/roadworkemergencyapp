//
//  User.m
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 4/18/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import "User.h"
#import <UIKit/UIKit.h>

#define DEFAULT_RADIUS 10 //km
static User *_user = nil;

@implementation User


+ (id)sharedUser
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _user = [[self alloc] init];
    });
    return _user;
}

- (id)init {
    if (self = [super init]) {
        _userid = 1;
        _username = @"wei";
        _deviceid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        _platformid = 1;
        _radius = DEFAULT_RADIUS;
    }
    return self;
}
@end
