//
//  RoadworkDetailTableViewController.h
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 5/14/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Roadwork, REHTTPRequest;

@interface RoadworkDetailTableViewController : UITableViewController
{
    
    IBOutlet UITextField *nameTextField;
    IBOutlet UITextField *delayTextField;
    IBOutlet UITextField *addressTextField;
    IBOutlet UITextField *locationTextField;
    IBOutlet UITextView *descriptionTextView;
    
    REHTTPRequest *httpRequest;
    
}

@property (nonatomic, strong) Roadwork* roadwork;
@property (nonatomic) BOOL editingMode;

@end
