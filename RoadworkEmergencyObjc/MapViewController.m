//
//  FirstViewController.m
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 4/18/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import "MapViewController.h"
#import "REHTTPRequest.h"
#import "User.h"
#import "REJSONParser.h"
#import "RoadworkPOI.h"
#import "GEOFencinCalculator.h"
#import "RoadworkDetailTableViewController.h"

#define DEFAULT_FENCE_RADIUS 1000 //meter

@interface MapViewController ()

@end

@implementation MapViewController

- (IBAction)refreshButtonPressed:(id)sender {
    
    [roadworkRequest fetchRoadworksFromLon:user.longitude Lat:user.latitude radius:user.radius];
}

-(void) detectRoadworks
{
    for (RoadworkPOI *roadworkPOI in roadworkPOIs)
    {
        CLCircularRegion *fenceRegion = roadworkPOI.fence;
        BOOL isOutOfFence = [GEOFencinCalculator isLocation:_locationManager.location outOfFence:fenceRegion];
        if (isOutOfFence)
        {
            NSLog(@"*User is out of the fence!!");
        }
        else
        {
            NSLog(@"*User in the fence.");
            [self showRoadworkNearbyAlert:roadworkPOI.roadwork];
            [self sendLocalNotification:roadworkPOI.title description:roadworkPOI.advice];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    user = [User sharedUser];
    alertedRoadworkIds = [NSMutableArray array];
    
    roadworkRequest = [[REHTTPRequest alloc] init];
    roadworkRequest.delegate = self;
    userlocationRequest = [[REHTTPRequest alloc] init];
    userlocationRequest.delegate = self;
    
    fences = [NSMutableArray array];
    
    roadworkPOIs = [[NSMutableArray alloc] init];
    firstTimeLoadMap = YES;
    
    
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;
    [_mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];
    
    [self startStandardUpdates];
    [self addGestureRecognizer];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) addGestureRecognizer
{
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(dropPin:)];
    longPressGestureRecognizer.minimumPressDuration = 1.2f;
    [_mapView addGestureRecognizer:longPressGestureRecognizer];
}

-(void) dropPin:(UILongPressGestureRecognizer*)longPressGestureRecognizer
{
    CGPoint touchPoint = [longPressGestureRecognizer locationInView:_mapView];
    CLLocationCoordinate2D coordinatePoint = [_mapView convertPoint:touchPoint toCoordinateFromView:_mapView];
    
    Roadwork *roadwork = [[Roadwork alloc] init];
    roadwork.point = coordinatePoint;
    [self presentRoadworkDetailView:roadwork editMode:YES];

    // Request roadwork
    //[roadworkRequest fetchRoadworksFromLon:user.longitude Lat:user.latitude radius:user.radius];

}

- (void)startStandardUpdates
{
    if (nil == _locationManager)
    {
        _locationManager = [[CLLocationManager alloc] init];
    }
    
    if(CLLocationManager.authorizationStatus == kCLAuthorizationStatusNotDetermined)
    {
        [_locationManager requestWhenInUseAuthorization];
    }
    
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // Set up location manager in background
    _locationManager.pausesLocationUpdatesAutomatically = NO;
    _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    
    // Set a movement threshold for new events.
    _locationManager.distanceFilter = 100; // meters
    
    [_locationManager startUpdatingLocation];
    CLLocation* location = _locationManager.location;
    user.latitude = location.coordinate.latitude;
    user.longitude = location.coordinate.longitude;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {

    CLLocation* location = newLocation;
    [self updateUserLocation:location];
}

-(void)updateUserLocation:(CLLocation*) location
{
    user.latitude = location.coordinate.latitude;
    user.longitude = location.coordinate.longitude;
}

-(RoadworkPOI*)createPOI:(Roadwork*)roadwork
{
    RoadworkPOI* annotation = [[RoadworkPOI alloc] init];

    annotation.coordinate = CLLocationCoordinate2DMake(roadwork.point.latitude,roadwork.point.longitude);
    annotation.title = roadwork.name;
    annotation.subtitle = roadwork.roadWorkDescription;
    annotation.advice = roadwork.advice;
    annotation.roadwork = roadwork;
    
    CLCircularRegion *fenceRegion = [self createRoadworkFence:roadwork.point];
    
    MKCircle *circleFence = [MKCircle circleWithCenterCoordinate:fenceRegion.center radius:fenceRegion.radius];
    [fences addObject:circleFence];
    [_mapView addOverlay:circleFence];
    
    annotation.fence = fenceRegion;
    
    return annotation;
}

-(CLCircularRegion*) createRoadworkFence:(CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D fenceCenter = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
    CLCircularRegion *fenceRegion =  [[CLCircularRegion alloc] initWithCenter:fenceCenter
                                                     radius:DEFAULT_FENCE_RADIUS
                                                 identifier:@"RoadworkFence"];
    return fenceRegion;
}

-(void) removeRoadworks
{
    [_mapView removeOverlays:fences];
    [_mapView removeAnnotations:roadworkPOIs];
    [roadworkPOIs removeAllObjects];
    [fences removeAllObjects];
}

-(void) addRoadworks:(NSArray*)roadworksToAdd
{
    [self removeRoadworks];
    
    for (Roadwork *roadwork in roadworksToAdd)
    {
        RoadworkPOI *poi = [self createPOI:roadwork];
        [roadworkPOIs addObject:poi];
        [_mapView addAnnotation:poi];
    }
}

-(void) sendLocalNotification:(NSString*)name description:(NSString*)advice
{
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.fireDate = [NSDate date];
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    localNotif.alertBody = [NSString stringWithFormat:@"Alet! Nearby roadwork!"];
    localNotif.alertAction = advice;
    localNotif.alertTitle = name;
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

-(BOOL) hasShownAlert:(long)roadworkIdtoCheck
{
    for (NSNumber *roadworkId in alertedRoadworkIds)
    {
        if ([roadworkId longValue] == roadworkIdtoCheck)
        {
            return YES;
        }
    }
    
    return NO;
}

-(void) showRoadworkNearbyAlert:(Roadwork*)roadwork
{
    if (roadwork)
    {
        if (![self hasShownAlert:roadwork.roadworkId])
        {
            [alertedRoadworkIds addObject:[NSNumber numberWithLong:roadwork.roadworkId]];
            NSString *name = roadwork.name;
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert! Nearby roadwork!" message:[NSString stringWithFormat:@"Nearby %@.",name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
        }
        
    }
    
}

- (IBAction)showCurrentLocation {
    [_mapView setCenterCoordinate:_locationManager.location.coordinate animated:YES];
}

#pragma mark - REHTTPRequestDelegate
-(void)request:(REHTTPRequest*)request receiveJSON:(NSDictionary *)json
{
    if (request == roadworkRequest)
    {
        roadworks = [REJSONParser parseRoadworks:json];
        [self addRoadworks:roadworks];
        [self detectRoadworks];
    }
    
}

#pragma mark - MKMapviewDelegate
-(void)mapView:(MKMapView *)mkmapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    //CLLocationCoordinate2D userCoordinate = userLocation.location.coordinate;
    [self detectRoadworks];
}

- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView
                       fullyRendered:(BOOL)fullyRendered
{
    if (firstTimeLoadMap)
    {
        [roadworkRequest fetchRoadworksFromLon:user.longitude Lat:user.latitude radius:user.radius];
        firstTimeLoadMap = NO;
    }
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[RoadworkPOI class]])
    {
        // Try to dequeue an existing pin view first.
        MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
            //pinView.animatesDrop = YES;
            pinView.canShowCallout = YES;
            pinView.image = [UIImage imageNamed:@"roadwork.png"];
            UIButton *detailBtn = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            CGRect frame = detailBtn.frame;
            frame.origin.y = pinView.frame.size.height / 2.f;
            detailBtn.frame = frame;
            pinView.rightCalloutAccessoryView = detailBtn;
        } else {
            pinView.annotation = annotation;
            UIButton *detailBtn = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            CGRect frame = detailBtn.frame;
            frame.origin.y = pinView.frame.size.height / 2.f;
            detailBtn.frame = frame;
            pinView.rightCalloutAccessoryView = detailBtn;
        }
        return pinView;
    }
    return nil;
}

- (MKOverlayView *)mapView:(MKMapView *)map viewForOverlay:(id <MKOverlay>)overlay
{
    MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
    circleView.strokeColor = [UIColor redColor];
    circleView.fillColor = [[UIColor colorWithRed:0.0f green:95.f/255 blue:246.f/255.f alpha:1] colorWithAlphaComponent:0.4];
    circleView.lineWidth = 3.0f;
    return circleView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if ([view.annotation isKindOfClass:[RoadworkPOI class]])
    {
        RoadworkPOI *roadworkPOI = (RoadworkPOI *)view.annotation;
        
        [self presentRoadworkDetailView:roadworkPOI.roadwork editMode:NO];
    }
}

-(void) presentRoadworkDetailView:(Roadwork*)roadwork editMode:(BOOL)editMode
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RoadworkDetailTableViewController *detailViewController = [storyboard instantiateViewControllerWithIdentifier:@"RoadworkDetailTableViewController"];
    detailViewController.editingMode = editMode;
    detailViewController.roadwork = roadwork;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
    [self presentViewController:navController animated:YES completion:nil];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    [self updateUserLocation:[locations lastObject]];
    [userlocationRequest postUserLocation];
    [roadworkRequest fetchRoadworksFromLon:user.longitude Lat:user.latitude radius:user.radius];
}

@end
