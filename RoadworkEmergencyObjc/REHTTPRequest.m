//
//  REGTTPRequest.m
//  RoadworkEmergencyObjc
//
//  Created by Syunwei on 4/18/15.
//  Copyright (c) 2015 Syunwei. All rights reserved.
//

#import "REHTTPRequest.h"
#import "User.h"
#import "AFHTTPRequestOperationManager.h"
#import "Roadwork.h"
#import "REJSONParser.h"


#define SERVER_URI @"http://58.173.165.193:8080/emergencynotificationserver"


@implementation REHTTPRequest

+(instancetype) request
{
    return [[self alloc] init];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        AFSecurityPolicy *policy = [[AFSecurityPolicy alloc] init];
        [policy setAllowInvalidCertificates:YES];
        user = [User sharedUser];

        _httpManager = [AFHTTPRequestOperationManager manager];
        [_httpManager setSecurityPolicy:policy];
        
        AFHTTPResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
        AFHTTPRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        _httpManager.requestSerializer = requestSerializer;//[AFJSONRequestSerializer serializer];
        _httpManager.responseSerializer = responseSerializer;//[AFJSONResponseSerializer serializer];
        
    }
    return self;
}

-(void) postToken:(NSString*) token
{
    NSString *url = [NSString stringWithFormat:@"%@/token",SERVER_URI];
    NSDictionary *parameters = @{@"userid": [NSNumber numberWithLong:user.userid], @"username":user.username, @"deviceid":user.deviceid, @"platformid":[NSNumber numberWithInt:user.platformid], @"token":token};
    [_httpManager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void) fetchRoadworksFromLon:(double)longitude Lat:(double)latitude radius:(float)radius
{
    NSString *url = [NSString stringWithFormat:@"%@/roadworks",SERVER_URI];
    NSDictionary *parameters = @{@"longitude":[NSNumber numberWithDouble:longitude], @"latitude":[NSNumber numberWithDouble:latitude], @"radius": [NSNumber numberWithDouble	:radius]};

    
    [_httpManager POST:url parameters:parameters
               success:
     ^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (_delegate)
         {
             NSLog(@"%@",responseObject);
             [_delegate request:self receiveJSON:responseObject];
         }

     }
               failure:
     ^(AFHTTPRequestOperation *operation, NSError *error)
    {
         NSLog(@"Error: %@", error);
    }];
    
}

-(void) postUserLocation
{
    NSString *url = [NSString stringWithFormat:@"%@/userlocation",SERVER_URI];
    NSDictionary *parameters = @{@"longitude":[NSNumber numberWithDouble:user.longitude], @"latitude":[NSNumber numberWithDouble:user.latitude], @"userid": [NSNumber numberWithLong:user.userid]};
    
    
    [_httpManager POST:url parameters:parameters
               success:
     ^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (_delegate)
         {
             NSLog(@"%@",responseObject);
             [_delegate request:self receiveJSON:responseObject];
         }
         
     }
               failure:
     ^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];
}

-(void)putRoadwork:(Roadwork*)roadwork
{
    NSString *url = [NSString stringWithFormat:@"%@/roadworks",SERVER_URI];

    NSDictionary *event = @{@"isHighImpact":[NSNumber numberWithBool:NO],@"description":(roadwork.roadWorkDescription ? roadwork.roadWorkDescription : [NSNull null]), @"type":@"User added",@"cause":[NSNull null],@"delay":(roadwork.delay ? roadwork.delay : [NSNull null]), @"advice":(roadwork.advice ? roadwork.advice : [NSNull null])};
    NSDictionary *properties = @{@"location":[NSNull null], @"event":event};
    NSDictionary *geometry = @{@"type":@"Point", @"coordinates":@[[NSNumber numberWithDouble:roadwork.point.longitude],[NSNumber numberWithDouble:roadwork.point.latitude]]};
    NSDictionary *source = @{@"name":(roadwork.name ? roadwork.name : [NSNull null])};
    NSDictionary *feature = @{@"type":@"Feature",@"id":[NSNull null],@"source":source, @"geometry":geometry, @"properties":properties};
    NSDictionary *features = @{@"features":@[feature]};
    
    [_httpManager PUT:url parameters:features
               success:
     ^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (_delegate)
         {
             NSLog(@"%@",responseObject);
             [_delegate request:self receiveJSON:responseObject];
         }
         
     }
               failure:
     ^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error.description);
     }];
}

@end
